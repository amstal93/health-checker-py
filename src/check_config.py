from config_loader import load_config
import sys

config_file = sys.argv[1]

try:
    load_config(config_file)
    print("Config file looks fine")
except BaseException as e:
    print(f"The config file ${config_file} can't be loaded. Please review its structure.")
    print(e)
