from model import Domain, SubdomainHealth
from log import LogLevel, log
from utils import build_url, get_status_code, download_and_check_string


def check_health(domain: Domain):
    log(LogLevel.INFO, f'Checking health of domain : {domain.url}')
    subdomains = domain.subdomains
    healths = []
    for subdomain in subdomains:
        log(LogLevel.INFO, f'Checking health of subdomain : {subdomain.uri}', indent=1)
        full_url = build_url(domain.ssl, domain.url, subdomain.uri)
        http_code = get_status_code(full_url)
        string_found = download_and_check_string(full_url, subdomain.expected_string)
        if (http_code == subdomain.expected_http_code) & string_found:
            health = 'OK'
        else:
            health = 'KO'
        healths.append(
            SubdomainHealth(
                subdomain=subdomain.name,
                health=health,
                full_url=full_url,
                expected_http_code=subdomain.expected_http_code,
                actual_http_code=http_code,
                expected_string=subdomain.expected_string,
                string_found=string_found
            )
        )
    return healths
