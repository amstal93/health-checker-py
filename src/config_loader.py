import yaml
from yaml.loader import SafeLoader
from model import Domain, Subdomain


def load_config(config_file):
    with open(config_file, 'r') as f:
        config = list(yaml.load_all(f, Loader=SafeLoader))[0]['watch']
        domains_str_list = list(config.keys())
        domains = []
        for domain_str in domains_str_list:
            domain = config[domain_str]
            url = domain_str
            ssl = domain['ssl']
            subdomains_str_list = list(domain['subdomains'])
            subdomains = []
            for subdomain_str in subdomains_str_list:
                subdomain = domain['subdomains'][subdomain_str]
                uri = subdomain['uri']
                expected_http_code = subdomain['expected-http-code']
                if 'expected-string' in subdomain:
                    expected_string = subdomain['expected-string']
                else:
                    expected_string = ''
                subdomains.append(
                    Subdomain(
                        uri=uri,
                        name=subdomain_str,
                        expected_http_code=expected_http_code,
                        expected_string=expected_string
                    )
                )
            domains.append(Domain(url=url, ssl=ssl, subdomains=subdomains))
        return domains
